import {noteListActionCreators} from './NoteList';

const createNoteRequestType = 'CreateNote_Request';
const createNoteResponseSuccessType = 'CreateNote_Response_Success';
const createNoteResponseErrorType = 'CreateNote_Response_Error';
const clearNoteType = 'Clear_Note';

const initialState = {note: null, isError: false, isloading : false};

export const createNoteActionCreators = {
    createNote: (note) => async (dispatch, getState) => {

        const requestBody = note;

        dispatch({ type: clearNoteType });

        dispatch({ type: createNoteRequestType });

        const url = `/api/note`;

        const response = await fetch(url,
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(requestBody)
            });

        if (response.ok) {
            const note = await response.json();     
            dispatch({ type: createNoteResponseSuccessType, note });

            dispatch(noteListActionCreators.addNoteToNoteList(note));
            
        } else {
            dispatch({ type: createNoteResponseErrorType });
        }   
      
    }
};

export const reducer = (state, action) => {

    state = state || initialState;

    if (action.type === createNoteRequestType) {
        return {
            ...state,
            isloading: true
        };
    }

    if (action.type === createNoteResponseSuccessType) {
        return {
            ...state,
            note: action.note,
            isloading: false
        };
    }

    if (action.type === createNoteResponseErrorType) {
        return {
            ...state,
            isError : true,
            isloading: false
        };
    }

    if (action.type === clearNoteType) {
        return {
            ...state,
            ...initialState
        };
    }

    return state;
};

