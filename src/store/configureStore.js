import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { connectRouter, routerMiddleware } from "connected-react-router";

import * as NoteList from './NoteList';
import * as CreateNote from './CreateNote';
import * as EditNote from './EditNote';
import * as DeleteNote from './DeleteNote';

export default function configureStore(history, initialState) {
    
    const reducers = {
      noteList: NoteList.reducer,
      createdNote: CreateNote.reducer,
      editedNote: EditNote.reducer,
      deletedNote: DeleteNote.reducer,
    }

    const middleware = [
        thunk,
        routerMiddleware(history)
      ];
    
      // In development, use the browser's Redux dev tools extension if installed
      const enhancers = [];
      const isDevelopment = process.env.NODE_ENV === 'development';
      if (isDevelopment && typeof window !== 'undefined' && window.devToolsExtension) {
        enhancers.push(window.devToolsExtension());
      }
    
      const rootReducer = combineReducers({  
        ...reducers,
        router: connectRouter(history),
        
      });
    
      return createStore(
        rootReducer,
        initialState,
        compose(applyMiddleware(...middleware), ...enhancers)
      );
}