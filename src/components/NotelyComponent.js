import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { noteListActionCreators } from '../store/NoteList'; 
import HeaderComponent from './notely-sub/HeaderComponent';
import BodyComponent from './notely-sub/BodyComponent';
import FooterComponent from './notely-sub/FooterComponent';

class NotelyComponent extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount(){
        this.props.fetchNotes();
    }

    render() {
        return (
            <div>
                <HeaderComponent/>
                <BodyComponent/>
                <FooterComponent/>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {

    };
}

function mapDispatchToProps(dispatch) {
    return  bindActionCreators({
        fetchNotes: noteListActionCreators.fetchNotes
     }, dispatch) 
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NotelyComponent);
