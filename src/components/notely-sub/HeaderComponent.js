import React from 'react';

function HeaderComponent(props) {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container">
                <a className="navbar-brand" href="/" style={{ fontSize: "30px", fontWeight: "500" }}>
                    <span style={{ color: "#008080" }}>Note</span><span style={{ color: "black" }}>ly</span>
                </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div className="navbar-nav mr-auto">
                        <a className="nav-item nav-link active" href="/" style={{ color: "#008080" }}>Home <span className="sr-only">(current)</span></a>
                    </div>
                    <div className="navbar-nav ml-auto">
                        <a className="nav-item nav-link" href="/create-note" style={{ color: "#008080" }}>New Note</a>
                    </div>
                </div>
            </div>
        </nav>
    );
}

export default HeaderComponent;
