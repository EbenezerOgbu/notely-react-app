import React from 'react';
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

function NoteDetailFooterComponent(props) {
    const noteId = (props.note !== undefined ? props.note.id:"")
    return (
        <div className="card-footer note-detail__footer">
            <div className="note-detail__flex-container">
                <div>
                    <Link className="note-detail__button" to={"/"}>
                        Back to List
                    </Link>
                </div>
                <div>
                    <Link className="note-detail__button" to={`/edit-note/${noteId}`}>
                        Edit Note
                     </Link>
                </div>
                <div>
                    <Link className="note-detail__button" to={""} onClick={() => { props.handleDelete(noteId) }}>
                        Delete Note
                    </Link>
                </div>
            </div>
        </div>
    );
}


NoteDetailFooterComponent.propTypes = {
    note: PropTypes.object
};

export default NoteDetailFooterComponent;
