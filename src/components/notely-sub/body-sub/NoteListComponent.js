import React from 'react';
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

function NoteListComponent(props) {
    return (
        <div className="container">
            <div className="row justify-content-md-center">
                <div style = {{marginTop:"60px", width:"80%"}}>                   
                    {manageNotes(props.notes)}
                    <div className="text-right index__new-note-button-wrapper">
                        <Link to={"/create-note"} className="index__new-note-button">New Note</Link>
                    </div>
                </div>
            </div>
        </div>
    );
}

function manageNotes(notes) {
    if (notes&&notes.length > 0) {
        return displayNotes(notes);
    }else{
        return displayEmptyListSnippet();
    }
}

function displayNotes(notes) {
    return (
        <div>
            <div className="card text-center index__header" style={{ marginBottom: "25px" }}>
                <div className="card-body" style={{ height: "80px" }}>Your Notes</div>
            </div>
            {notes.map((note, index) =>
                <div key = {index} >
                    <div className="card" style={{ marginBottom: "10px" }} >
                        <Link to={`/note-detail/${note.id}`} className="index__list-item-link">
                            <div className="card-body">
                                {note.subject}
                            </div>
                        </Link>
                    </div>
                </div>
            )}
        </div>
    )
}

function displayEmptyListSnippet() {
    return (
        <div className="card text-center index__empty-list">
            <div className="card-body">
                You don't have any notes. Click on the button below to create a note.
            </div>
        </div>
    )
}

NoteListComponent.propTypes = {
    notes : PropTypes.arrayOf(PropTypes.object)
};

export default NoteListComponent;
