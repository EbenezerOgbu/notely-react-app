import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Link} from "react-router-dom";
import { bindActionCreators } from 'redux';
import { createNoteActionCreators } from '../../../store/CreateNote'; 

class NewNoteComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            subject: "",
            body: "",
            displayFieldError: false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const target = event.target;
        if (target.id ==="subject") {
            this.setState({ subject: target.value });
        } else {
            this.setState({ body: target.value });
        }
    }
    
    handleSubmit() {
        if (this.isPageValid()) {
            let subject = this.state.subject;
            let body = this.state.body;
            let newNote = { subject: subject, detail: body }
            this.props.createNote(newNote);
            this.props.history.push("/");
        } else {
            this.setState({ displayFieldError : true });
        }
    }

    isPageValid() {
      return this.state.subject !== "";
    }


    render() {
        return (
            <div className="container">
                <div className="note-editor">
                    <div className="card note-editor__panel">
                        <div className="card-header note-editor__header">
                            <h1 className="card-title note-editor__panel-title">
                                <span>Create New Note</span>
                            </h1>
                        </div>
                        <form>
                            <div className="card-body note-editor__panel-body">
                                <div className="form-group">
                                    <label className="note-editor__input-label">Subject</label>
                                    <input className="form-control note-editor__subject-input"
                                        id="subject"
                                        onChange={(event) => {this.handleChange(event)}} />
                                </div>

                                <div className="form-group">
                                    <label className="note-editor__input-label" >Your Note</label>
                                    <textarea className="form-control note-editor__detail-input"
                                        id="body"
                                        rows="12"
                                        cols="20"
                                        onChange={this.handleChange}></textarea>
                                </div>
                            </div>
                            <div className="card-footer">
                                <div className="note-editor__flex-container">
                                    <Link className="btn btn-sm note-editor__button" to={"/"}>Cancel</Link>
                                    <button className="btn btn-sm note-editor__button" type="button" onClick={() => this.handleSubmit()}>Save Note</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}


function mapStateToProps(state) {
    return {

    };
}

function mapDispatchToProps(dispatch) {
    let bindResult = bindActionCreators({createNote: createNoteActionCreators.createNote}, dispatch);
    return bindResult
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NewNoteComponent);
