import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { deleteNoteActionCreators } from '../../../store/DeleteNote'; 
import NoteDetailHeaderComponent from './note-detail-sub/NoteDetailHeaderComponent';
import NoteDetailBodyComponent from './note-detail-sub/NoteDetailBodyComponent';
import NoteDetailFooterComponent from './note-detail-sub/NoteDetailFooterComponent';

class NoteDetailComponent extends Component {
    constructor(props) {
        super(props)
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete(noteId) {
        this.props.deleteNote(noteId);
        this.props.history.push("/");
    }

    render() {
        const note = getNoteFromList(this.props);
        return (
            <div className = "container">
            <div className="note-detail">
                <div className="card note-detail__panel">
                    <NoteDetailHeaderComponent note = {note}/>
                    <NoteDetailBodyComponent note = {note}/>
                    <NoteDetailFooterComponent note = {note} handleDelete={this.handleDelete}/>
                </div>
            </div>
        </div>
        );
    }
}

function getNoteFromList(props) {
    let notes = props.notes;
    if (notes.length > 0) {
        let id = props.match.params.id;
        let note = notes.find(note => note.id==id);
        return note;
    }
}


function mapStateToProps(state) {
    return {

    };
}

function mapDispatchToProps(dispatch) {
    return  bindActionCreators({
        deleteNote: deleteNoteActionCreators.deleteNote
    }, dispatch)  
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NoteDetailComponent);
